package com.example.demo;

import com.springboot.kafkaproducer.Bookings;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;

public class randomMessage {

    public static Bookings.CabServices randomMessageGenerator(){
        Bookings.CabServices.Builder msg = Bookings.CabServices.newBuilder();
        Bookings.Vehicle.Builder veh = Bookings.Vehicle.newBuilder();
        Bookings.Driver.Builder drive = Bookings.Driver.newBuilder();
        drive.setName(RandomStringUtils.randomAlphabetic(8));
        drive.setLicense(RandomStringUtils.randomAlphanumeric(7));
        drive.setId(Integer.parseInt(RandomStringUtils.randomNumeric(2)));
        drive.setPhone(RandomStringUtils.randomNumeric(10));
        drive.setEmail(RandomStringUtils.randomAlphanumeric(8)+"@gmail.com");
        Bookings.Driver.Address.Builder add1 = Bookings.Driver.Address.newBuilder();
        add1.setAddressline1(RandomStringUtils.randomAlphabetic(4));
        add1.setAddressline2(RandomStringUtils.randomAlphabetic(7));
        add1.setCity(RandomStringUtils.randomAlphabetic(5));
        add1.setState(RandomStringUtils.randomAlphabetic(6));
        drive.setAdd(add1);
        veh.setModel(RandomStringUtils.randomAlphabetic(5));
        veh.setVehicleNum(RandomStringUtils.randomAlphanumeric(10));
        veh.setDriver1(drive);
        int a=(Integer.parseInt(RandomStringUtils.randomNumeric(1)))%4;
        veh.setVtype(Bookings.VehicleType.valueOf(a));
        List<Bookings.Vehicle> list = new ArrayList<>();
        list.add(veh.build());
        msg.addAllRegVehicle(list);
        Bookings.CabServices msg1 = msg.build();
        return msg1;
        //System.out.println(msg1);
    }
}
