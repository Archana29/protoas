package com.example.demo;

import com.springboot.kafkaproducer.Bookings;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Properties;
@EnableScheduling
public class Producer {
    @Scheduled(fixedRate = 5000)
    public static void producer(){
        Properties properties = new Properties();
        properties.put("bootstrap.servers","localhost:9092");
        properties.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer","org.apache.kafka.common.serialization.ByteArraySerializer");
        randomMessage msg = new randomMessage();
        KafkaProducer<String, byte[]> kafkaProducer = new KafkaProducer<>(properties);
        Bookings.CabServices message = msg.randomMessageGenerator();
        kafkaProducer.send(new ProducerRecord<>("test",message.toByteArray()));
        kafkaProducer.close();
    }
}
